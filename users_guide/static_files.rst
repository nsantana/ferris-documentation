Static Files
============

Static files live in ``app/static`` and can be accessed via ``/static``.

The folders ``css``, ``js``, and ``img`` are aliased and can be accessed via ``/css``, ``/js``, and ``/img`` respectively.

Plugin assets live in ``plugins/<plugin_name>/static`` and are available at ``/plugins/<plugin_name>/``.
