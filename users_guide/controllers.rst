Controllers
===========

.. module:: ferris.core.controller

As you may have guessed, Controllers are the 'C' in MVC. They are responsible for processing and responding to HTTP requests. Controllers contain methods called Actions that respond to requests, and are typically lightweight classes that glue the :doc:`models` to the :doc:`templates`. 

Although controllers are typically associated with a single model, it's completely possible to use multiple or no models at all. Usually you'll want to put most of your business logic in your models and simply write glue code in the controller. Controllers can be extended with :doc:`components` and :doc:`scaffolded <scaffolding>`.

The routing layer determines which Controller and Action to call when a request is made.

    .. autoclass:: Controller()


A Controller that deals with time travel might look like this::
   
    from ferris import Controller, route

    class Tardis(Controller):
        class Meta:
            prefixes = ('admin',)
            components = (Extrapolator, Time)

        def list(self):
            return 'We are currently at %s' % self.components.time.current()

        @route
        def set(self, time):
            self.components.time.set(time)
            return "Time set to %s" % self.components.time.current()

        @route
        def admin_self_destruct(self):
            start_self_destruct_sequence()
            return "Run!"

The implementation of the "Extrapolator" and "Time" components is left as an exercise for the reader.
 

Conventions
-----------

Controllers are named plural nouns in upper camel case (UpperCamelCase) from the models that they are associated with (for example: Pages, Users, Images, Bears, etc.). There are many cases where the plural convention doesn't make sense, such as controllers that don't have an associated model or controllers that span multiple models.

Each controller class should be in its own file under ``/app/controllers`` and the name of the file should be the underscored class name. For example, to create a controller to act on fuzzy bears, create the file ``/app/controller/fuzzy_bears.py``. Inside, define a class named ``FuzzyBears``.

This section focuses on actions and how to process and respond to requests. Other features are tied into controllers in various ways. Actions are exposed via the :doc:`routing` system. Controllers can automatically render a :doc:`view <views>`, process POST and JSON data and attach it to :doc:`forms` or :doc:`messages` using :doc:`request_parsers`. Finally, controllers also allow you to break out common functionality using :doc:`components`.


Configuration
-------------

    .. autoclass:: ferris.core.controller::Controller.Meta()
        :members:

This Meta class is also used to configure components::

    class Meta:
        components = (Pagination,)
        pagination_limit = 5

The Meta class is constructed and made available via ``self.meta`` (lowercase). You can use this to change configuration on the fly::

    @route
    def list_long(self):
        self.meta.pagination_limit = 50
        self.meta.change_view('json')


Actions
-------

Actions are normal instance methods that can be invoked via HTTP. Actions are responsible for receiving requests and generating a response. 

Ferris takes care of :doc:`automatically routing <routing>` actions and generating URLS. The CRUD actions list, view, add, edit, and delete are automatically routed. Other actions have to be explicitly marked for routing.

A simple action might look like this::

    # via /controller/echo/<text>
    @route
    def echo(self, text):
        return text


Requests
--------

Actions can access the current request using ``self.request``::

    def list(self):
        return self.request.path

For more information on the request object see the `webapp2 documentation on requests <http://webapp-improved.appspot.com/guide/request.html>`_.

Data
~~~~

Actions can also access the GET and POST variables using ``self.request.params``::

    # i.e. /controller?text=meow
    def list(self):
        return self.request.params['text']

For just GET variables use ``self.request.GET``, and for POST only use ``self.request.POST``. PUT and PATCH data are always in self.request.POST.

More complex request data such as :doc:`forms` and :doc:`messages` instance handled using :doc:`request_parsers`.

Parameters
~~~~~~~~~~~

Actions can also take various parameters on the URL as described in :doc:`routing`::

    # /controller/concat/<text>/<number>
    @route
    def concat(self, text, number):
        return text + str(number)

User & Session
~~~~~~~~~~~~~~

.. autoattribute:: Controller.user

.. autoattribute:: Controller.session


For example::

    def user_profile():
        if not 'profile' in self.session:
            self.session['profile'] = UserProfile.find_by_user(self.user)
        return self.session['profile']


Route Info
~~~~~~~~~~

The controller provides all of the information about the current route via ``self.route``. The purpose of these variables is explained in depth in :doc:`routing`.

.. attribute:: Controller.route

.. attribute:: Controller.route.action
    
    The current action, such as 'add', 'list', 'edit', etc.

.. attribute:: Controller.route.prefix

    The current prefix, such as None, 'admin', 'api', etc.

.. attribute:: Controller.route.controller

    The current controller's name.

.. attribute:: Controller.route.name

    The canonical route name, as generated by :ref:`routing-url-and-name-generation`.

.. attribute:: Controller.route.args

    Any positional arguments passed inside of the route's url template.

.. attribute:: Controller.route.kwargs

    Any keyword arguments passed inside of the route's url template.


Response
--------

Ferris simplifies responding by allowing you to return plain types that get transformed into responses or just allowing the Views to auto-render. However, it's often useful to directly access the response to set headers or output binary data.  Actions can directly access the current response using ``self.response``::

    def list(self):
        self.response.headers['x-test'] = "greeting"
        self.response.write('hi')
        return self.response

For more information on the request object see the `webapp2 documentation on responses <http://webapp-improved.appspot.com/guide/response.html>`_.


Return Values
~~~~~~~~~~~~~

Ferris uses :doc:`response_handlers` to transform anything returned from an action into a response. 

Actions can return a string and the string will become the body of the response with the content-type 'text/html'::

    def list(self):
        return 'Hi!'

You can set the content-type before hand if you'd like::

    def list(self):
        self.response['content-type'] = 'text/plain'
        return 'plain, plain, old text.'

Actions can return an integer and the will become the status of the response, in this case the response will be a `404 Not Found`::

    def list(self):
        return 404

Actions can return any ``webapp2.Response`` class, including ``self.response``::

    def list(self):
        self.response.content_type = 'application/json'
        self.response.text = '[0,1,2]'
        return self.response

Even if you return a string or integer, any changes to ``self.response`` are kept (except for the body or status, respectively)::

    def list(self):
        self.response.content_type = 'text/xml'
        self.response.headers['cache-control'] = 'nocache'
        return '<x>Hello!</x>'

Returning nothing (``None``) will trigger the automatic view rendering unless ``self.meta.view.auto_render`` is set to ``False``. See :doc:`views` for more information::

    def list(self):
        # nothing returned, so by default 'app/templates/controller/list.html' will be rendered.
        pass


Redirection
~~~~~~~~~~~

Redirects can be generated using ``self.redirect`` and :meth:`~Controller.uri`::
    
    @route
    def auto(self):
        return self.redirect(self.uri(action='exterminate', who='everything'))

Security
--------

Controllers are secured using :doc:`authorization_chains`.


Utilities
---------

    .. autoclass:: ferris.core.controller::Controller.Util()
        :members:


``decode_key`` is especially useful for working with urlsafe keys::
    
    @route
    def one(self):
        item = Widget.find_by_name('screwdriver')
        return self.redirect(
            self.uri(action='two', key=item.key.urlsafe()))

    @route
    def two(self, key):
        item = self.util.decode_key(key).get()
        return item.name


The Startup Method and Events
-----------------------------

Handlers have various :doc:`events` that are called during the lifecycle of a request. Event handlers should be registered at the beginning of a request using the startup callback method.

    .. automethod:: Controller.startup()

You can tap into these events using :attr:`Controller.events` which is a :class:`~ferris.core.event.NamedEvents` instance::


    def startup(self):
        self.events.before_dispatch += self.on_after_dispatch

    def self.on_after_dispatch(self, controller=None, response=None):
        logging.info('hello!')


Events
~~~~~~

For a usual request, the events in order are:

#. setup_template_variables
#. before_build_components, after_build_components
#. before_startup, after_startup
#. before_dispatch, after_dispatch
#. template_names (only if using :class:`~ferris.core.views.TemplateView`)
#. before_render, after_render (only if a view is rendered)
#. dispatch_complete

These events are broadcast to the global event bus with the prefix ``controller_``.
