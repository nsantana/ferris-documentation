Settings
========

.. module:: ferris.core.settings

The settings module provides a way to specify application-wide settings in a centralized registry. You can also use the special dynamic settings plugin you can configure your settings via the Ferris admin interface.


Configuration
-------------

To configure settings use ``app/settings.py``. There should already be some defaults configured but you may add your own::

    settings['cats'] = {
        'herdable': False
    }


To read your settings, import ``ferris.settings`` and use :func:`get`::

    from ferris import settings

    HERDABLE = settings.get('cats').get('herdable')

.. warning:: Do not import ``app.settings`` directly. Always use the settings API as it allows for things such as the dynamic settings plugin to work correctly and ensure that everything is loaded in the right order.


Functions
---------

.. autofunction:: get

.. autofunction:: settings


Dynamic Settings Plugin
------------------------

.. module:: plugins.settings

The built-in settings plugin provides you with the ability to configure overrides to the default settings via the ferris admin interface.

To use the settings plugin make sure it's enabled in ``app/routes.py`` (preferably before any other plugins)::
    
    plugins.enable('settings')

Then, activate the plugin inside of ``app/settings.py``::

    import plugins.settings
    plugins.settings.activate(settings)

If you're using plugins that expose additional dynamic settings (such as the service account plugin), be sure to import those *before* activating the settings::

    import plugins.settings
    import plugins.service_account.settings
    plugins.settings.activate(settings)


.. warning:: If you import additional settings after activating the plugin, the settings dictionary will not be properly updated.

You can now access the settings manager at `/admin/settings <http://localhost:8080/admin/settings>`_.

Any settings specified in the admin interface will take precendence over the ones specified in your ``settings.py`` file.

You can also add your own custom settings to the admin interface. To do so, subclass the settings model :class:`~plugins.settings.SettingsModel`::

    from ferris import ndb
    from plugins.settings import SettingsModel


    class SFTPSettings(SettingsModel):
        _name = 'SFTP Settings'
        _settings_key = 'sftp_settings'
        _description = 'SFTP Settings'
        server = ndb.StringProperty(required=True)
        user_name = ndb.StringProperty(required=True)
        password = ndb.StringProperty(required=True)

This is a normal :class:`model <ferris.core.ndb.Model>` class so regular ndb properties are used to specify fields. You must specify the :attr:`~Setting._setting_key` and :attr:`~Setting._name` attributes.

.. class:: SettingsModel

.. attribute:: Setting._setting_key

    The key where the settings will be stored. To get these settings use ``settings.get(_setting_key)``.

.. attribute:: Setting._name

    The name to display for this setting in the admin interface.

.. attribute:: Setting._description
    
    The description to use for this setting in the admin interface.
