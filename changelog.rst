Changelog
=========

Version 2.1.0
-------------

  * The template debugger has been removed.
  * Various style (pep8) errors fixed.
  * Added the ability to get cloud storage information from uploads, see :doc:`users_guide/uploads_and_downloads`.
  * Settings API overhaul. See :doc:`users_guide/settings`.
  * GeoPtProperties now correctly translated when using wtforms.
  * Fixed error with /_ah/warmup requests always failing.
  * Added the ability to return a response from an authorization chain.
  * Search now correctly indexes repeated properties.
  * OAuth2 service accounts are now backed by datastore storage so they do not refresh the access token too frequently.
  * OAuth2 integration now uses the (new) ndb utilies built in to oauth2client.
  * The path-patching code has been moved into ``ferris/fix_imports.py`` and is now loaded directly by ``appengine_config.py``.


Migration from 2.0.x
~~~~~~~~~~~~~~~~~~~~

  1. Update the ``ferris``, ``packages``, and ``plugins`` directory.
  2. Update ``/main.py``, remove ``/fix_imports.py``, add ``/appengine_config.py``.
  3. Carefully update ``/app.yaml`` being sure to preserve the application id and version as well as any custom handlers.
  4. Translate existing app settings from ``/settings.py`` to the new format in ``/app/settings.py``. See :doc:`users_guide/settings`. Remove ``/settings.py``.
  5. Make sure any additional settings classes in your custom plugins are imported into ``/app/settings.py``.
  6. Remove any references in your template to "elements/debug/toolbox.html".


Version 2.0.1
-------------

2.0.1 is a patch release and is thus backwards-compatible with 2.0.0. No migration should be needed.

  * Fixing issue where value formatter did not work for repeated keys.
  * 400/500 errors now return JSON when using the messaging API.
  * Fixing issue where oauth2 credentials could be duplicated.
  * Added priority events (no surface API change).
  * Updating third-party packages
  * Removed gaepytz in favor of the maintained pytz-appengine
  * Adding default tabindex to form fields.
  * Moved all testing tools to `ferrinose <https://bitbucket.org/jonparrott/ferrisnose>`_ and made aliases to ensure backwards-compatibility.
  * Added the download controller, see :doc:`users_guide/uploads_and_downloads`.

Version 2.0.0
-------------

  * Major overhaul of the controller part of Ferris' MVC. We've wholesale replaced handlers with the new :doc:`users_guide/controllers`. 
  * Third-party packages have been updated to newer versions.
  * New admin layout.
  * Bootstrap updated from 2 to 3.
  * :doc:`users_guide/scaffolding`  has been completely re-done. It's now a component and doesn't involve magic to make it work.
  * New scaffold views.
  * EasyHandler has been removed. With the new scaffold it's very easily to replicate its functionality.
  * Template tester plugin as been removed.
  * Scaffold only supports urlsafe keys, no more direct support for IDs.
  * New Cache component.
  * Memcache utilties have been improved for usability.
  * Protorpc :doc:`users_guide/messages` integration for REST APIs.
  * :doc:`users_guide/search` API integration.
  * Value formatters for pretty-printing in templates.
  * Request parsers for a unified API for processing Forms and Messages.
  * Response handlers to transform return values into responses.
  * View classes to help generate complex responses.
  * Test runner has been broken out into Ferrisnose.
  * Pagination overhauled with better support for tracking cursors.
  * TinyMCE has been removed (a seperate repository called ferris-fancy-forms is available).
  * Template events
  * Authorization chains
  * Removing our custom wtforms.appengine.ext.ndb module in favor of the built-in wtforms one.
  * Mail component removed, use ferris.core.mail instead.
  * app.yaml cleanup, Ferris is now included into app.yaml.
  * Custom app for handling deferred tasks, prevents import errors.
  * Plugin to manage OAuth2 service accounts.
  * JSON component removed in favor of Messages.
  * Higher test coverage.
  * Fixing various memory leaks.
  * Various bug fixes.
