Ferris Documentation
====================

This repository contains the source files for building the Ferris Documentation website. The documentation
is generated using sphinx and written in ReStructuredText.

Building
--------

In order to build on your machine, you must have the App Engine SDK and Ferris checked-out locally. Ferris must
be the same target version as the documentation.

 * Configure your paths by creating a `settings.yaml`
 * Run `sudo pip install sphinx_rtd_theme`
 * Run `make html`
 * Open `/_build/html` and run `python -m SimpleHTTPServer` to view the results.

Example settings.yaml
---------------------

Something like this, maybe:

    app-engine-sdk-path: "/home/paul/bin/google_appengine"
    ferris-framework-path: "/home/paul/workspace/Ferris/ferris-framework"
